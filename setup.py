# https://python-packaging.readthedocs.io/en/latest/dependencies.html
# https://packaging.python.org/guides/distributing-packages-using-setuptools/#install-requires

from setuptools import setup

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='ical2any',
      version='0.2',
      description='Convert icalendar files to other formats.',
      url='',
      author='Matthew Junker',
      author_email='junker@alum.mit.edu',
      license='MIT',
      packages=['ical2any'],
      python_requires='>=3.6',
      install_requires=[
#          'python-dateutil',
          'icalendar',
          'recurring_ical_events'
          ],
      entry_points={
          'console_scripts':['ical2any=ical2any.command_line:main']
          },
      zip_safe=False)
