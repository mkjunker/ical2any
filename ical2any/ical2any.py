#!/usr/bin/env python3
'''
Time-stamp: <Aug 17, 2023, 3:22 pm, Matthew K. Junker>

Convert ics files to Org and GCAL formats.
'''
import argparse
import logging
import sys
import datetime
import icalendar
import recurring_ical_events

minver = 3,8
args = None

def parse_args(argv):
    '''Parse arguments, set up logging.'''
    global args

    parser = argparse.ArgumentParser(
        description='')

    verbose_help = '-hh' in argv

    # See argparse.FileType

    parser.add_argument('-hh', action='help',
                        help="show verbose help and exit")
    parser.add_argument('--file', '-f',
                        type=str)
    parser.add_argument('--before', '-b',
                        type=int,
                        default=366)
    parser.add_argument('--after', '-a',
                        type=int,
                        default=366*3)
    parser.add_argument('--org', '-o',
                        action='store_true')
    parser.add_argument('--gcal', '-g',
                        action='store_true')
    parser.add_argument(
        '--nba',
        action='store_true',
        help='Make ini output for Bucks schedules')
    parser.add_argument(
        '--no-end',
        action='store_true',
        help='Skip end times')
    parser.add_argument('-hm',
                        action='store_true',
                        required=False,
                        help=('Module help' if verbose_help
                              else argparse.SUPPRESS))
    loglevels = list(logging._levelToName.values())
    loglevels.remove('NOTSET')
    loglevels = list(map(str.lower, loglevels))
    parser.add_argument('--log', '-l',
                        required=False,
                        metavar='LEVEL',
                        choices=loglevels,
                        help=('set logging level: ' + str(loglevels)
                              + ' [%(default)s]' if verbose_help
                              else argparse.SUPPRESS),
                        default='warning')
    parser.add_argument('--log-file', '-lf',
                        required=False,
                        help=('Log file name, '
                              'in addition to stderr'
                              if verbose_help
                              else argparse.SUPPRESS),
                        metavar='PATH')

    args = parser.parse_args(args=argv)
    level = getattr(logging, args.log.upper())
    handlers = [logging.StreamHandler(sys.stderr)]
    if args.log_file is not None:
        handlers.append(logging.FileHandler(args.log_file))
    logging.basicConfig(level=level,
                        handlers=handlers)
    if args.hm:
        help(__name__)
        sys.exit(0)

def checkminver():
    '''Insure the needed python version is used.

If an insufficient version is used, exit with code 1.'''
    if sys.version_info < minver:
        logging.critical(
            'Minimum python version is '+'.'.join(map(str,minver)))
        logging.critical(
            'Using '+'.'.join(map(str,sys.version_info)))
        sys.exit(1)


def resolve(e):
    diff = e['DTEND'].dt - e['DTSTART'].dt
    if isinstance(e['DTSTART'].dt, datetime.datetime):
        fullday = False
        fmt = '<{:%Y-%m-%d %a %H:%M}>'
    else:
        fmt = '<{:%Y-%m-%d %a}>'
        fullday = True

    try:
        start = e['DTSTART'].dt.astimezone()
    except:
        start = e['DTSTART'].dt
    try:
        end = e['DTEND'].dt.astimezone()
    except:
        end = e['DTEND'].dt

    oneday = False
    if fullday:
        if (diff.days == 1
            or (diff.days == 0 and diff.seconds == 0)):
            oneday = True
        else:
            end = end - datetime.timedelta(days=1)

    return start, end, fullday, oneday, diff.days


def org(events):
    for e in events:
        start, end, fullday, oneday, days = resolve(e)

        fmt = '<{:%Y-%m-%d %a}>' if fullday else '<{:%Y-%m-%d %a %H:%M}>'

        print('* ' + e['SUMMARY'])
        print('  :PROPERTIES:')
        loc = e.get('LOCATION', '')
        if loc.strip() != '':
            print(f'  :LOCATION: {loc}')
        print('  :END:')
        print('  ' + fmt.format(start), end='')
        if not oneday and not args.no_end:
            print('--' + fmt.format(end), end='')
        print()
        if e.get('DESCRIPTION', '').strip() != '':
            print(e['DESCRIPTION'])
        if loc.strip() != '':
            print()
            print('LOCATION: ' + loc)

def gcal(events):
    for e in events:
        leader = ' ..........' if args.file == 'info.ics' else ''

        start, end, fullday, oneday, days = resolve(e)

        if oneday:
            print('{:%Y%m%d}'.format(start) + leader, end='')
        elif fullday:
            print('{:%Y%m%d}:{}'.format(start, days) + leader, end='')
        else:
            if leader == '':
                leader = ' '
            print('{:%Y%m%d}{}{:%H:%M}-{:%H:%M}'.
                  format(start, leader, start, end), end='')


        print(' ' + e['SUMMARY'])


def nba(events):
    for e in events:

        start, end, fullday, oneday, days = resolve(e)

        if oneday:
            print('{:%Y%m%d}'.format(start) + leader, end='')
        elif fullday:
            print('{:%Y%m%d}:{}'.format(start, days) + leader, end='')
        else:
            dt = f'{start:%Y-%m-%d=%I:%M}'.replace('=0', '=').replace(':00', '')
            print(dt, end='')
        summ = e['SUMMARY']
        summ = summ.replace('Bucks at', '@')
        summ = summ.replace(' at Bucks', '')
        print(' ' + summ)


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    parse_args(argv)
    checkminver()

    with open(args.file, 'r') as f:
        ic = icalendar.Calendar.from_ical(f.read())
        start = datetime.datetime.now() - datetime.timedelta(
            days=args.before)
        end = datetime.datetime.now() + datetime.timedelta(
            days=args.after)
        events = recurring_ical_events.of(ic).between(start, end)
        if args.org:
            org(events)
            return
        if args.gcal:
            gcal(events)
        if args.nba:
            nba(events)


if __name__ == "__main__":
    sys.exit(main())
